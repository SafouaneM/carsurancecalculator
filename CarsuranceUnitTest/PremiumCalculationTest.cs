using System;
using Xunit;
using WindesheimAD2021AutoVerzekeringsPremie.Implementation;
using System.Reflection;


namespace CarsuranceUnitTest
{
    
    public class PremiumCalculationTest
    {
        
        [Fact]
        public void PremiumPaymentAmountYearTest()
        {
            var vehicle = new Vehicle(180, 24000, 2005);
            var policyHolderWa = new PolicyHolder(21, "31-01-2004", 4500, 0);
            
            var premiumWa = new PremiumCalculation(vehicle, policyHolderWa, InsuranceCoverage.WA);
            
            var expectedValue = Math.Round(premiumWa.PremiumAmountPerYear * 0.975,2);
            
            Assert.Equal(expectedValue, premiumWa.PremiumPaymentAmount(PremiumCalculation.PaymentPeriod.YEAR));
        }
        
        [Theory]
        [InlineData(100,800, 100 * 1.02)]
        [InlineData(100,4550,100)]
        [InlineData(100,3599,100 * 1.05)]
        [InlineData(100,4499,100 * 1.02)]
        [InlineData(100,999,100 * 1.02)]
        [InlineData(100,4000, 100 * 1.02)]
        [InlineData(100,2600, 100 * 1.05)]
        public void UpdatePremiumForPostalCodeTest(double premium, int postalCode, double expectation)
        {
            var Actualpremium = typeof(PremiumCalculation)
                .GetMethod("UpdatePremiumForPostalCode", BindingFlags.NonPublic | BindingFlags.Static)
                .Invoke(null, new Object[] {premium, postalCode});
            
            
            Assert.Equal(expectation, Actualpremium);
        }

        [Theory]
        [InlineData(100,3,100)]
        [InlineData(100,4,100)]
        [InlineData(100,5,100)]
        [InlineData(100,15,50)]
        [InlineData(100,25,35)]
        [InlineData(100,21,35)]
        [InlineData(100,19,35)]
        public void UpdatePremiumForNoClaimYearsTest(double premium, int years, double expected)
        {

            var ActualNoClaimPercentage = typeof(PremiumCalculation)
                .GetMethod("UpdatePremiumForNoClaimYears",
                    BindingFlags.Static | BindingFlags.NonPublic)
                .Invoke(null, new Object[] {premium, years});
            
            //Assert
            Assert.Equal(ActualNoClaimPercentage,expected);
        }
        
        [Theory]
        [InlineData(100,20000,2010,(209 / 3D))]
        [InlineData(100,40000,2003,(402 / 3D))]
        [InlineData(100,30000,2018,(317/ 3D))]
        public void CalculateBasePremiumTest(int PowerInKw,int ValueInEuros,int constructionYear,double expected)
        {
            //Arrange
            var Vehicle = new Vehicle(PowerInKw, ValueInEuros, constructionYear);
            
            //Act
            double actual = PremiumCalculation.CalculateBasePremium(Vehicle);
            
            //Assert
            Assert.Equal(expected,actual);
        }
        
        
       
        
    }
}