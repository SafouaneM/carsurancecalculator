using System;
using Xunit;
using WindesheimAD2021AutoVerzekeringsPremie.Implementation;
using System.Reflection;


namespace CarsuranceUnitTest
{
    public class VehicleTest
    {
        
        [Theory]
        [InlineData(160, 45000, 2015, 6)]
        [InlineData(270, 90000, 2018, 3)]
        [InlineData(270, 90000, 2022, 0)]
        public void CheckIfAgeIsZeroWhenConstructionYearIsGreaterThanDateTime(int PowerInKw, int ValueInEuros, int constructionYear, int expected)
        {
            
            //Arrange
            Vehicle vehicle = new(PowerInKw, ValueInEuros, constructionYear);

            //Act
            int actual = vehicle.Age;
            
            //Assert
            Assert.Equal(expected, actual);
        }
        
        //DateTime.Now.Year - constructionYear;  <-- Doe dat if needed
        
        
        
    }
}